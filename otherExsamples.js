// import React, { useState, useEffect, useReducer } from 'react';
// import ReactDOM from 'react-dom';
// import reportWebVitals from './reportWebVitals';

// const App = (props) => {
//     const [count, setCount] = useState(props.count)
//     const [text, setText] = useState('')
//
//     useEffect(() => {
//         console.log('This should only run once!')
//     }, [])
//
//     useEffect(() => {
//         console.log('useEffect ran')
//         document.title = count
//     }, [count])
//
//     // const increment = () => {
//     //     setCount(count + 1)
//     // }
//
//     return (
//         <div>
//             <input value={text} onChange={(e) => setText(e.target.value)} />
//             <p>The current {text || 'count'} is {count}</p>
//             {/*<button onClick={increment}>+1</button>*/}
//             <button onClick={() => setCount(count + 1)}>+1</button>
//             <button onClick={() => setCount(props.count)}>Rest</button>
//             <button onClick={() => setCount(count - 1)}>-1</button>
//         </div>
//     )
// }
//
// App.defaultProps = {
//     count: 0
// }

// Fails
// const App = (props) => {
//     const [state, setState] = useState({
//         count: 0,
//         text: ''
//     })
//
//     return (
//         <div>
//             <input value={state.text} onChange={(e) => setState({ text: e.target.value })} />
//             <p>The current {state.text || 'count'} is {state.count}</p>
//             {/*<button onClick={increment}>+1</button>*/}
//             <button onClick={() => setState({ count: state.count + 1 })}>+1</button>
//             <button onClick={() => setState({ count: props.count })}>Rest</button>
//             <button onClick={() => setState({ count: state.count - 1 })}>-1</button>
//         </div>
//     )
// }

// ReactDOM.render(<App count={0} />, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();